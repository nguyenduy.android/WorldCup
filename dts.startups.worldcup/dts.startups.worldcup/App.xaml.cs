﻿using System;
using Xamarin.Forms;
using dts.startups.worldcup.Views;
using Xamarin.Forms.Xaml;
using dts.startups.worldcup.Extensions;
using dts.startups.worldcup.Views.Main;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace dts.startups.worldcup
{
    public partial class App : Application
    {
        public static string CurrentLanguage = "EN";

        public App()
        {
            InitializeComponent();

            DetectLanguage();

            var bootstrapper = new AppBootstrapper();
            MainPage = bootstrapper.CreateMainPage();
        

        }

        private void DetectLanguage()
        {
            // This lookup NOT required for Windows platforms - the Culture will be automatically set
            if (Xamarin.Forms.Device.RuntimePlatform == Xamarin.Forms.Device.iOS || Xamarin.Forms.Device.RuntimePlatform == Xamarin.Forms.Device.Android)
            {
                // determine the correct, supported .NET culture
                var ci = DependencyService.Get<ILocalization>().GetCurrentCultureInfo();
                Resx.AppResources.Culture = ci; // set the RESX for resource localization
                DependencyService.Get<ILocalization>().SetLocale(ci); // set the Thread for locale-aware methods
            }
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}

