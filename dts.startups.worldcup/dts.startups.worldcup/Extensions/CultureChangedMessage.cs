﻿using System.Globalization;

namespace dts.startups.worldcup.Extensions
{
    public class CultureChangedMessage
    {
        public CultureInfo NewCultureInfo { get; private set; }

        public CultureChangedMessage(string lngName) 
            : this(new CultureInfo(lngName))
        {
            //TODO
        }

        public CultureChangedMessage(CultureInfo newCultureInfo)
        {
            NewCultureInfo = newCultureInfo;
        }
    }
}
