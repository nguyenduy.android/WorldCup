﻿using dts.startups.worldcup.Models;
using dts.startups.worldcup.ViewModels;
using dts.startups.worldcup.Views.Main;
using ReactiveUI;
using ReactiveUI.XamForms;
using Splat;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace dts.startups.worldcup
{
    public class AppBootstrapper : ReactiveObject, IScreen
    {
        public RoutingState Router { get; set; }
        public AppBootstrapper()
        {
            Router = new RoutingState();
            Locator.CurrentMutable.RegisterConstant(this, typeof(IScreen));
            Locator.CurrentMutable.Register(() => new WorldCupPage(), typeof(IViewFor<WorldCupPageMenuItem>));

            this
                .Router
                .NavigateAndReset
                .Execute(new WorldCupPageMenuItem())
                .Subscribe();


        }
        public Page CreateMainPage()
        {
            // NB: This returns the opening page that the platform-specific
            // boilerplate code will look for. It will know to find us because
            // we've registered our AppBootstrappScreen.
            return new RoutedViewHost();
        }
    }
}
