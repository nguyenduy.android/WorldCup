﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace dts.startups.worldcup.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MainPage : TabbedPage
	{
		public MainPage ()
		{
			InitializeComponent ();
		}
	}
}