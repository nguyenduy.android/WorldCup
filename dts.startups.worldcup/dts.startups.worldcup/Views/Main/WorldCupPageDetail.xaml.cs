﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace dts.startups.worldcup.Views.Main
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class WorldCupPageDetail : ContentPage
    {
        public WorldCupPageDetail()
        {
            InitializeComponent();
        }
    }
}