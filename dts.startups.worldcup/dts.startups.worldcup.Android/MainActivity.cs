﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using XLabs.Ioc;
using XLabs.Platform.Device;
using FFImageLoading;
using FFImageLoading.Forms.Droid;

namespace dts.startups.worldcup.Droid
{
    [Activity(Label = "dts.startups.worldcup", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            PreInit();
            global::Xamarin.Forms.Forms.Init(this, bundle);
            LoadApplication(new App());
        }
        private void PreInit()
        {
            if (!Resolver.IsSet)
            {
                this.SetIoc();
            }

            InitFFImageLoading();
        }
        private void SetIoc()
        {
            var resolverContainer = new SimpleContainer();
            resolverContainer.Register<IDevice>(t => AndroidDevice.CurrentDevice);
            Resolver.SetResolver(resolverContainer.GetResolver());
        }
        private void InitFFImageLoading()
        {
            FFImageLoading.Forms.Platform.CachedImageRenderer.Init(true);

            var config = new FFImageLoading.Config.Configuration()
            {
                VerboseLogging = false,
                VerbosePerformanceLogging = false,
                VerboseMemoryCacheLogging = false,
                VerboseLoadingCancelledLogging = false,
                Logger = new CustomLogger(),
            };
            ImageService.Instance.Initialize(config);
        }

    }
    public class CustomLogger : FFImageLoading.Helpers.IMiniLogger
    {
        public void Debug(string message)
        {
            Console.WriteLine(message);
        }

        public void Error(string errorMessage)
        {
            Console.WriteLine(errorMessage);
        }

        public void Error(string errorMessage, Exception ex)
        {
            throw new NotImplementedException();
        }
    }
}

